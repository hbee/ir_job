#-*- coding:utf-8 -*-

from openerp.osv import osv

class IrModelData(osv.Model):
    _inherit = 'ir.model.data'

    def _process_end(self, cr, uid, modules):
        res = super(IrModelData, self)._process_end(cr, uid, modules)
        self.pool.get('ir.job').cancel_progress_jobs(cr, uid)
        return res
