#-*- coding:utf-8 -*-

import time
import traceback
import sys
from openerp.osv import fields, osv
from loglevels import ustr
import logging
from ast import literal_eval

_logger = logging.getLogger(__name__)

class IrJob(osv.Model):
    _name = 'ir.job'
    _columns = {
        'name': fields.char('Name', size=128, required=True),
        'ref': fields.text('Ref'),
        'model': fields.char('Model', size=256, required=True),
        'user_id': fields.many2one('res.users', 'User', required=True),
        'start_date': fields.datetime('Start Date'),
        'end_date': fields.datetime('End Date'),
        'state': fields.selection([('in_progress', 'In Progress'),
               ('done', 'Done'),
               ('error', 'Error'),
               ('cancelled', 'Cancelled')], 'State', required=True, select=True),
        'error': fields.char('Error', size=256),
        'traceback': fields.text('Traceback'),
        'result_email': fields.char('Result E-Mail'),
        'result_tpl_success_id': fields.many2one('email.template', 'Success Template'),
        'result_tpl_error_id': fields.many2one('email.template', 'Error Template'),
        'result_source_model': fields.boolean('Use Source Model In E-Mail')
    }
    _order = 'start_date desc'
    _defaults = {
        'state': lambda *a: 'in_progress',
        'result_tpl_success_id': lambda s, cr, uid, context:\
            s.pool.get('ir.model.data').get_object_reference(cr, uid, 'ir_job',
                                                'email_template_job_report')[1],
        'result_tpl_error_id': lambda s, cr, uid, context:\
            s.pool.get('ir.model.data').get_object_reference(cr, uid, 'ir_job',
                                                'email_template_job_report')[1]
    }

    def cancel_progress_jobs(self, cr, uid, context=None):
        job_ids = self.search(cr, uid, [('state', '=', 'in_progress')],
                              context=context)
        self.write(cr, uid, job_ids, {'state': 'cancelled'}, context=context)
        return True

    def job_start(self, cr, uid, job_id, context=None):
        context = context or {}
        vals = {
            'start_date': time.strftime('%Y-%m-%d %H:%M:%S'),
        }
        self.write(cr, uid, [job_id], vals, context=context)
        return True

    def register_job(self, cr, uid, name, model_name, ref, count=0, context=None):
        context = context or {}
        kwargs = literal_eval(ref.split('|')[2])
        job_data = {
            'name': name,
            'model': model_name,
            'ref': ref,
            'user_id': uid,
            'state': 'in_progress',
            'result_email': kwargs.get('result_email', False),
            'result_source_model': kwargs.get('result_source_model', False)
        }
        if 'result_tpl_success_id' in kwargs:
            job_data['result_tpl_success_id'] = kwargs['result_tpl_success_id']
        if 'result_tpl_error_id' in kwargs:
            job_data['result_tpl_error_id'] = kwargs['result_tpl_error_id']

        return self.create(cr, uid, job_data, context=context)

    def job_done(self, cr, uid, job_id, context=None):
        context = context or {}
        vals = {
            'end_date': time.strftime('%Y-%m-%d %H:%M:%S'),
            'state': 'done',
        }
        self.write(cr, uid, [job_id], vals, context=context)
        self.send_job_report(cr, uid, job_id, 'done', context)
        return True

    def job_exception(self, cr, uid, job_id, e, context=None):
        error_msg = '%s %s' % (ustr(e.name), ustr(e.value))
        tb_s = reduce(lambda x, y: x + y, traceback.format_exception(sys.exc_type,
                   sys.exc_value, sys.exc_traceback))
        vals = {
            'state': 'error',
            'error': error_msg,
            'traceback': tb_s,
        }
        self.write(cr, uid, [job_id], vals, context=context)
        self.send_job_report(cr, uid, job_id, 'error', context)
        _logger.exception("Job (%s) was terminated by error:\n%s" %  \
                          (error_msg, tb_s))
        return True

    def send_job_report(self, cr, uid, job_id, status, context=None):
        if context is None:
            context = {}

        email_obj = self.pool.get('email.template')

        job = self.browse(cr, uid, job_id, context)
        if not job.result_email:
            return False

        obj_id = job_id
        if job.result_source_model:
            obj_id = literal_eval(job.ref.split('|')[1])[0]

        email_tpl_id = job.result_tpl_success_id.id
        if status == 'error':
            email_tpl_id = job.result_tpl_error_id.id

        if email_tpl_id:
            context['job'] = job
            email_id = email_obj.send_mail(cr, uid, email_tpl_id, obj_id,
                                           force_send=True, context=context)

            return email_id

        return False
