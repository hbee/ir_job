# -*- coding: utf-8 -*-

from threading import Thread, Lock
from functools import wraps
import pooler
from copy import copy
from Queue import Queue

common_queue = Queue()
stdout_lk = Lock()

class JobThread(Thread):
    def __init__(self, target, name, pool, model, cr, uid, *args, **kwargs):
        self._target = target
        self._job_model = pool.get('ir.job')
        self._model = model
        self._cr = cr
        self._uid = uid
        self._args = args
        self._kwargs = kwargs
        self._callback_done = kwargs.pop('callback_done', False)
        self._callback_error = kwargs.pop('callback_error', False)
        model_name = model._name
        ref = "%s|%s|%s" % (target.__name__, args, kwargs)
        self._job_id = self._job_model.register_job(cr, uid, name, model_name, ref)
        cr.commit()

        Thread.__init__(self)

    def run(self):
        try:
            stdout_lk.acquire()
            self._job_model.job_start(self._cr, self._uid, self._job_id)
            self._cr.commit()
            self._target(self._model, self._cr, self._uid, *self._args,
                         **self._kwargs)
            # Assume for now that self._args[1] is context
            # TODO: Do something better
            context = {}
            if len(self._args) > 1 and self._args[1]:
                context = self._args[1]
            self._cr.commit()
            self._job_model.job_done(self._cr, self._uid, self._job_id, context)
            self._cr.commit()
            if self._callback_done:
                self._callback_done(self._model, self._cr, self._uid,
                                    self._args[0])
                self._cr.commit()
        except Exception as e:
            try:
                self._job_model.job_exception(self._cr, self._uid, self._job_id,
                                              e)
                self._cr.commit()
                if self._callback_error:
                    self._callback_error(self._model, self._cr, self._uid,
                                         self._args[0])
                    self._cr.commit()
            except:
                self._cr.rollback()
        finally:
            self._cr.close()
            del self._target, self._uid, self._model, self._job_model, \
                self._cr, self._args, self._kwargs, self._callback_done, \
                self._callback_error

            stdout_lk.release()

def job(name, callback_done=False, callback_error=False):
    def job_decorator(func):
        @wraps(func)
        def job_func(model, cr, uid, *args, **kwargs):
            name1 = copy(name)
            kwargs['callback_done'] = callback_done
            kwargs['callback_error'] = callback_error
            db, pool = pooler.get_db_and_pool(cr.dbname)
            model_name = model._name
            obj_ids = args[0]
            obj_name = pool.get(model_name).name_get(cr, uid, obj_ids)
            if obj_name and obj_name[0]:
                name1 += ' ' + obj_name[0][1]
            t = JobThread(func, name1, pool, model, db.cursor(), uid, *args,
                          **kwargs)
            t.start()
            return {
                'name': '%s job started' % name1,
                'view_mode': 'form',
                'type': 'ir.actions.act_window',
                'res_model': 'ir.job.started.wizard',
                'target': 'new',
            }
        return job_func
    return job_decorator
