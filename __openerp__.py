#-*- coding:utf-8 -*-
# @author Paulius Sladkevičius <paulius@hbee.eu>

{
    "name" : "Job Quote",
    "version" : "1.1",
    "author" : "HBEE",
    "website" : "www.hbee.eu",
    "category" : "Base",
    "depends" : ["base", "email_template"],
    "update_xml" : [
        "data/job_email_template.xml",
        "view/ir_job.xml",
        "wizard/started.xml",
        "security/ir.model.access.csv",
    ],
    "description": """ Job Quote system

    Jobs are stored in the Settings>Configuration->Job->Jobs

    Example of usage:
    from openerp.addons.ir_job.thread import job

    @job('Job Name')
    def func(self, cr, uid):
        pass
    """,
    "init_xml" : [],
    "demo_xml": [],
    "installable" : True,
    "active" : False,
}
